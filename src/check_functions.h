#ifndef _CHECK_FUNCTIONS_H_
#define _CHECK_FUNCTIONS_H_

#include "can-ids/Devices/TE_CAN.h"
#include "torque_encoder.h"

unsigned int    check_GND_VCC                   (unsigned int signal, unsigned int select_sensors);
void            check_sensors                   (TE_VALUES *value, TE_CORE *status);
unsigned int    check_sensors_integrity         (TE_VALUES *value, TE_CORE *status);

void            check_implausibility            (TE_VALUES *value, TE_CORE *status);

unsigned int    find                            (unsigned int signal1, unsigned int signal2, unsigned int select_sensor);
unsigned int    check_5_25_APPS_BPPS            (TE_VALUES *value, unsigned int select_plasibility);
void            check_APPS_BPPS_plausibility    (TE_VALUES *value, TE_CORE *status);

void            check_master                    (TE_VALUES *value, TE_CORE *status, COUNTERS *timer_counter);

unsigned int    increment_counter               (unsigned int flag, unsigned int counter);
unsigned int    check_counter                   (unsigned int flag, unsigned int counter);
void            check_time                      (TE_CORE *status, COUNTERS *timer_counter); 

void            check_AIR_line                  (TE_CORE *status);

bool check_TE_RTD_status (TE_CORE *status, TE_VALUES *value);


#endif