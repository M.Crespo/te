#ifndef _UPDATE_FUNCTIONS_H
#define _UPDATE_FUNCTIONS_H

#include "torque_encoder.h"

#include "can-ids/Devices/TE_CAN.h"

typedef enum{
    APPS_0,
    APPS_1,
    BPS_ELECTRIC,
    BPS_PRESSURE_0,
    BPS_PRESSURE_1
    
}SENSOR_SELECT;

typedef enum{
    SHORT_CIRCUIT,
    OVERSHOOT,
    NORMAL

}POSITION_SELECT;

typedef enum{
    LOWER,
    UPPER

}SAFE_ZONE_SELECT;

void update_selected_mechanical_thresholds (INTERFACE_MSG_PEDAL_THRESHOLDS select, 
                                            TE_VALUES *value,
                                            _prog_addressT EE_address, 
                                            uint16_t new_mechanical_thresholds[16],
                                            TE_THRESHOLDS *thresholds);

void check_threshold_overlap (SAFE_ZONE_SELECT safe_zone, SENSOR_THRESHOLDS *thresholds, uint16_t *raw_average);
void init_mechanical_thresholds (SENSOR_THRESHOLDS *thresholds);

void update_thresholds(INTERFACE_MSG_PEDAL_THRESHOLDS select, TE_THRESHOLDS *thresholds);

void update_ranges              (TE_THRESHOLDS *thresholds);
void update_values              (TE_VALUES *value);
void update_raw_averages        (TE_VALUES *value);



void update_treated_averages (TE_VALUES *value, TE_THRESHOLDS *thresholds, TE_CORE *status);

void update_sensor_value (SENSOR_SELECT sensor,
                          uint16_t *course, 
                          SENSOR_THRESHOLDS *thresholds, 
                          TE_CORE *status);
                          
void update_sensor_status (SENSOR_SELECT sensor, POSITION_SELECT position ,TE_CORE *status);

#endif