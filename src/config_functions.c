#include "config_functions.h"
#include "update_functions.h"

#include "lib_pic30f/ADC.h"




void init_adc (void)
{
    ADC_parameters config;

    config.idle                     = 1;
    config.form                     = 0;
    config.conversion_trigger       = AUTO_CONV;
    config.sampling_type            = AUTO;
    config.voltage_ref              = INTERNAL;
    config.sample_pin_select_type   = AUTO;
    config.smpi                     = 4;
    config.sampling_time            = 31;
    config.conversion_time          = 63;
    config.pin_select               = AN3 | AN4 | AN9 |AN10 | AN11;
    config.interrupt_priority       = 5; 
    config.turn_on                  = 1;

    TRISBbits.TRISB3 = 1;	//AN3 input     APPS_0
    TRISBbits.TRISB4 = 1;	//AN4 input     APPS_1
    TRISBbits.TRISB9 = 1;   //AN9 input     BPS_pressure_1
    TRISBbits.TRISB10 = 1;  //AN10 input    BPS_electric_0
    TRISBbits.TRISB11 = 1;  //AN11 input    BPS_pressure_0

    config_adc (config);

    return;
}


void init_thresholds (_prog_addressT EE_address, TE_THRESHOLDS *thresholds, uint16_t EE_thresholds[16])
{
    eeprom_read_row (EE_address, EE_thresholds);

    thresholds->APPS_0.gnd = APPS_0_GND_THRS;
    thresholds->APPS_0.vcc = APPS_0_VCC_THRS;
    thresholds->APPS_0.lower_mechanical = EE_thresholds[APPS_0_LOWER_MEC];
    thresholds->APPS_0.upper_mechanical = EE_thresholds[APPS_0_UPPER_MEC];
    thresholds->APPS_0.zero_force = thresholds->APPS_0.lower_mechanical + ZERO_FORCE_DEADZONE;
    thresholds->APPS_0.max_force  = thresholds->APPS_0.upper_mechanical - MAX_FORCE_DEADZONE;

    thresholds->APPS_1.gnd = APPS_1_GND_THRS;
    thresholds->APPS_1.vcc = APPS_1_VCC_THRS;
    thresholds->APPS_1.lower_mechanical = EE_thresholds[APPS_1_LOWER_MEC];
    thresholds->APPS_1.upper_mechanical = EE_thresholds[APPS_1_UPPER_MEC];
    thresholds->APPS_1.zero_force = thresholds->APPS_1.lower_mechanical + ZERO_FORCE_DEADZONE;
    thresholds->APPS_1.max_force  = thresholds->APPS_1.upper_mechanical - MAX_FORCE_DEADZONE;

    thresholds->BPS_electric.gnd = BPS_ELECTRIC_GND_THRS;
    thresholds->BPS_electric.vcc = BPS_ELECTRIC_VCC_THRS;
    thresholds->BPS_electric.lower_mechanical = EE_thresholds[BPS_ELECTRIC_LOWER_MEC];
    thresholds->BPS_electric.upper_mechanical = EE_thresholds[BPS_ELECTRIC_UPPER_MEC];
    thresholds->BPS_electric.zero_force = thresholds->BPS_electric.lower_mechanical + ZERO_FORCE_DEADZONE_BPS_ELECTRIC;
    thresholds->BPS_electric.max_force  = thresholds->BPS_electric.upper_mechanical - MAX_FORCE_DEADZONE_BPS_ELECTRIC;

    thresholds->BPS_pressure_0.gnd = BPS_PRESSURE_0_GND_THRS;
    thresholds->BPS_pressure_0.vcc = BPS_PRESSURE_0_VCC_THRS;
    thresholds->BPS_pressure_0.lower_mechanical = EE_thresholds[BPS_PRESSURE_0_LOWER_MEC];
    thresholds->BPS_pressure_0.upper_mechanical = EE_thresholds[BPS_PRESSURE_0_UPPER_MEC];
    thresholds->BPS_pressure_0.zero_force = thresholds->BPS_pressure_0.lower_mechanical + ZERO_FORCE_DEADZONE;
    thresholds->BPS_pressure_0.max_force  = thresholds->BPS_pressure_0.upper_mechanical - MAX_FORCE_DEADZONE;

    thresholds->BPS_pressure_1.gnd = BPS_PRESSURE_1_GND_THRS;
    thresholds->BPS_pressure_1.vcc = BPS_PRESSURE_1_VCC_THRS;
    thresholds->BPS_pressure_1.lower_mechanical = EE_thresholds[BPS_PRESSURE_1_LOWER_MEC];
    thresholds->BPS_pressure_1.upper_mechanical = EE_thresholds[BPS_PRESSURE_1_UPPER_MEC];
    thresholds->BPS_pressure_1.zero_force = thresholds->BPS_pressure_1.lower_mechanical + ZERO_FORCE_DEADZONE;
    thresholds->BPS_pressure_1.max_force  = thresholds->BPS_pressure_1.upper_mechanical - MAX_FORCE_DEADZONE;

    thresholds->APPS_0.range         = thresholds->APPS_0.max_force         - thresholds->APPS_0.zero_force;
    thresholds->APPS_1.range         = thresholds->APPS_1.max_force         - thresholds->APPS_1.zero_force;
    thresholds->BPS_pressure_0.range = thresholds->BPS_pressure_0.max_force - thresholds->BPS_pressure_0.zero_force;
    thresholds->BPS_pressure_1.range = thresholds->BPS_pressure_1.max_force - thresholds->BPS_pressure_1.zero_force;
    thresholds->BPS_electric.range   = thresholds->BPS_electric.max_force   - thresholds->BPS_electric.zero_force;

    init_mechanical_thresholds(&thresholds->APPS_0);
    init_mechanical_thresholds(&thresholds->APPS_1);
    init_mechanical_thresholds(&thresholds->BPS_electric);
    init_mechanical_thresholds(&thresholds->BPS_pressure_0);
    init_mechanical_thresholds(&thresholds->BPS_pressure_1);

    return;
}