#include <xc.h>

#include "check_functions.h"
#include <stdlib.h>


/*
*********************************************************************
|   Name:           check_GND_VCC
|				
|	Description: 	Checks if any of the variables overshoots any of
|                   the defined safety thresholds
|
|   Priority:       - 
|
|	Arguments:	    The value of signal and the sensor selector number
|
|	Returns:	    0 - Threshold was overshot, 1 - Sensor value is OK
|
|   Tested:         Yes
*********************************************************************
*//*
unsigned int check_GND_VCC (unsigned int signal, unsigned int select_sensors) 
{    
    switch (select_sensors)
    {
        case 0:
            if ((signal <= threshold.BPS_electric.gnd) || (signal >= threshold.BPS_electric.vcc)) 
                return 1;
            else 
                return 0;
            break;
        
        case 1:
            if ((signal <= threshold.BPS_pressure_0.gnd) || (signal >= threshold.BPS_pressure_0.vcc)) 
                return 1;
            else 
                return 0;
            break;

        case 2:
            if ((signal <= threshold.BPS_pressure_1.gnd) || (signal >= threshold.BPS_pressure_1.vcc)) 
                return 1;
            else 
                return 0;
            break; 
            
        case 3:
            if ((signal <= threshold.APPS_0.gnd) || (signal >= threshold.APPS_0.vcc)) 
                return 1;
            else 
                return 0;
            break;

        case 4:
            if ((signal <= threshold.APPS_1.gnd) || (signal >= threshold.APPS_1.vcc)) 
                return 1;
            else 
                return 0;
            break;

        default:
            return 0;        
    }
}
    
*/
/*
*********************************************************************
|   Name:           check_sensors
|				
|	Description: 	Checks if all sensors's values are within the
|                   thresholds
|
|   Priority:       - 
|
|	Arguments:	    Pointer to the structure containing the values
|                   Pointer to the status structure
|
|	Returns:	    -
|
|   Tested:         Yes
*********************************************************************
*//*
void check_sensors (TE_VALUES *value, TE_CORE *status) 
{        
    if (check_GND_VCC (value->avg_BPS_electric_0, 0))
        status->CC_BPS_electric_0 = 1;
    else 
        status->CC_BPS_electric_0 = 0;
    
    if (check_GND_VCC (value->avg_BPS_pressure_0, 1))
        status->CC_BPS_pressure_0 = 1;
    else
        status->CC_BPS_pressure_0 = 0;
    
    if (check_GND_VCC (value->avg_BPS_pressure_1, 2))
        status->CC_BPS_pressure_1 = 1;
    else
        status->CC_BPS_pressure_1 = 0;

    if (check_GND_VCC (value->avg_APPS_0, 3))
        status->CC_APPS_0 = 1;
    else    
        status->CC_APPS_0 = 0;

    if (check_GND_VCC (value->avg_APPS_1, 4))
        status->CC_APPS_1 = 1;
    else
        status->CC_APPS_1 = 0;

    return;
}
*/

/*
*********************************************************************
|   Name:           check_sensors_integrity
|				
|	Description: 	Performs a series of checks regarding the sensors's
|                   values and the safety thresholds
|
|   Priority:       - 
|
|	Arguments:	    Pointer to the structure containing the values
|                   Pointer to the status structure
|
|	Returns:	    0 - Something NOT OK, 1 - Everything OK
|
|   Tested:         Yes
*********************************************************************
*/
/*unsigned int check_sensors_integrity (TE_VALUES *value, TE_CORE *status)
{
    check_sensors (value, status);
    
    if (status->CC_BPS_pressure_0 == 1 && status->CC_BPS_pressure_1 == 1)   //  If both pressure sensors are NOT OK, imediatly return 0
        return 0;

    else if (status->CC_APPS_0 == 0 && status->CC_APPS_1 == 0)              //  If at least one pressure sensors is OK and both APPS are OK return 1
        return 1;
    
    else                                                                    //  Otherwise return 0, meaning that at least one of the APPS is NOT OK
        return 0;
}
*/

/*
*********************************************************************
|   Name:           check_implausibility
|				
|	Description: 	Checks if the APPS's values are implausible
|                   
|   Priority:       - 
|
|	Arguments:	    Pointer to the structure containing the values
|                   Pointer to the status structure
|
|	Returns:	    -
|
|   Tested:         Yes
*********************************************************************
*/
void check_implausibility (TE_VALUES *value, TE_CORE *status)
{
    if (abs(value->avg_APPS_0 - value->avg_APPS_1) >= 1000)
        status->Implausibility_APPS = 1;

    else
        status->Implausibility_APPS = 0;
    
    return;
}


/*
*********************************************************************
|   Name:           find
|				
|	Description: 	From two values:
|                       -Returns the smallest if it is an APPS
|                       -Returns the biggest if it is a BPS
|                   
|   Priority:       - 
|
|	Arguments:	    The value of two signals and the selection "bit"
|
|	Returns:	    (In the description)
|
|   Tested:         Yes
*********************************************************************
*/
unsigned int find (unsigned int signal1, unsigned int signal2, unsigned int select_sensor)
{
    switch (select_sensor)
    {
        case 0:     //BRAKE
            if (signal1 > signal2)
                return signal1;
            else 
                return signal2;
            break;
        
        case 1:     //ACCELERATOR
            if (signal1 > signal2)
                return signal2;
            else
                return signal1;
            break;
        
        default:
                return 0;
    }  
}



//tested
unsigned int check_5_25_APPS_BPPS (TE_VALUES *value, unsigned int select_plausibility)
{
    unsigned int brake = find (value->avg_BPS_pressure_0, value->avg_BPS_pressure_1, 0);
    unsigned int accelerator = find (value->avg_APPS_0, value->avg_APPS_1, 1);

    switch (select_plausibility)
    {
        case 0:     //25% PLAUSIBILITY CHECK
            if ((brake > 500) && (accelerator > 2500))
                return 1;
            else
                return 0;
            break;

        case 1:     //5% PLAUSIBILITY CHECK
            if (accelerator < 500)
                return 1;
            else
                return 0;
            break;
        
        default:    //DEFAULT TO 0 SINCE IT DOESNT MATTER
            return 0;
    }   
}

//tested
void check_APPS_BPPS_plausibility (TE_VALUES *value, TE_CORE *status)
{
    if (status->Implausibility_APPS_BPS == 0) 
    {
        if (check_5_25_APPS_BPPS(value, 0))
            status->Implausibility_APPS_BPS = 1;
    }
    else 
        if (check_5_25_APPS_BPPS(value, 1))
            status->Implausibility_APPS_BPS = 0;

    return;
}

////////////////////////////////////////////////////////////////////////////////

//tested
unsigned int increment_counter (unsigned int timer_flag, unsigned int counter)
{
    if (timer_flag)
    {
        if (counter >= 65500)
            counter = NUMBER_OF_COUNTERS;
        else
            counter++;
    }
    else
        if (counter > 0)
            counter = 0;
        
    return counter;
}

//tested
unsigned int check_counter (unsigned int timer_flag, unsigned int counter) 
{  
    if (timer_flag && counter >= NUMBER_OF_COUNTERS) 
        return 1;
    else 
        return 0;
}
    
//tested
void check_time (TE_CORE *status, COUNTERS *timer_counter) 
{
    timer_counter->APPS = increment_counter (status->Implausibility_APPS, timer_counter->APPS);
    
    status->Implausibility_APPS_Timer_Exceeded = check_counter (status->Implausibility_APPS, timer_counter->APPS);
    
    return;
}

////////////////////////////////////////////////////////////////////////////////////

//tested
void check_master (TE_VALUES *value, TE_CORE *status, COUNTERS *timer_counter) 
{        
    check_implausibility (value, status);
    
    check_time (status, timer_counter);

    check_APPS_BPPS_plausibility (value, status);

    check_AIR_line (status);

    return;
}

///////////////////////////////////////////////////////////////////////////////////////

/*
*********************************************************************
|   Name:           check_AIR_line
|				
|	Description: 	
|   Priority:       - 
|
|	Arguments:	    Pointer to the structure containing the values
|	Returns:	    -
|
|   Tested:         No
*********************************************************************
*/

void check_AIR_line (TE_CORE *status)
{
    if (PORTDbits.RD4)
        status->SC_Motor_Interlock_Front = 1;
    else 
        status->SC_Motor_Interlock_Front = 0;

    return;
}
    
bool check_TE_RTD_status (TE_CORE *status, TE_VALUES *value){

    if(    status->CC_APPS_0 == false         
        && status->CC_APPS_1 == false        
        && status->CC_BPS_electric_0 == false 
        && status->CC_BPS_pressure_0 == false 
        && status->CC_BPS_pressure_1 == false 
        && status->Implausibility_APPS_Timer_Exceeded == false 
        && status->Implausibility_APPS_BPS == false 
        
        && value->accelarator == 0
        && value->brake_pressure > 500){

        return true;
    }   
    else return false;
}

