#ifndef _GENERAL_PURPOSE_FUNCTIONS_H
#define _GENERAL_PURPOSE_FUNCTIONS_H

#include "torque_encoder.h"

typedef enum {OFF, ON} INTERRUPT_TOGGLE;

void acquire_values  (volatile bool *flag, TE_VALUES *values);
void proccess_values (volatile bool *flag, 
                      TE_VALUES *values, 
                      TE_CORE *status,
                      TE_THRESHOLDS *thresholds,
                      COUNTERS *time_counters);

void toggle_interrupts (INTERRUPT_TOGGLE toggle);
void toggle_adc_int    (INTERRUPT_TOGGLE toggle);
void toggle_timer1_int (INTERRUPT_TOGGLE toggle);

#endif