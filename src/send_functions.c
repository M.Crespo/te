#include "send_functions.h"

#include "update_functions.h"
#include "check_functions.h"

void send_BPS_eletric (TE_VALUES *value, TE_CORE *status, TE_MESSAGE *main_message)
{
    if (status->CC_BPS_electric_0 == 1)
    {
        main_message->BPS_electric = 0;
    }
        
    else 
    {
        main_message->BPS_electric = value->avg_BPS_electric_0;
    }
    
    return;
}

void send_BPS_pressure (TE_VALUES *value, TE_CORE *status, TE_MESSAGE *main_message)
{
    if (status->CC_BPS_pressure_0 == 0 && status->CC_BPS_pressure_1 == 0){
        //to be changed once in the car
        main_message->BPS_pressure = (float) ((value->avg_BPS_pressure_0 + value->avg_BPS_pressure_1) / 2);
    }
    else if (status->CC_BPS_pressure_0 == 1 && status->CC_BPS_pressure_1 == 0){

        main_message->BPS_pressure = value->avg_BPS_pressure_1;
    }
    else if (status->CC_BPS_pressure_0 == 0 && status->CC_BPS_pressure_1 == 1){

        main_message->BPS_pressure = value->avg_BPS_pressure_0;
    }   
    else{
        main_message->BPS_pressure = 0;
    } 
        
    return;
}

void send_APPS (TE_VALUES *value, TE_CORE *status, TE_MESSAGE *main_message)
{
    if (status->CC_APPS_0 == 0 && 
        status->CC_APPS_1 == 0 && 
        status->Implausibility_APPS_Timer_Exceeded == 0 && 
        status->Implausibility_APPS_BPS == 0 && 
        (status->CC_BPS_pressure_0 == 0 || status->CC_BPS_pressure_1 == 0)){

        main_message->APPS = value->accelarator;
    }
    else{
        main_message->APPS = 0;
    }

    return;
}

void send_status (TE_CORE *status, TE_MESSAGE *main_message)
{
    main_message->status.TE_status = status->TE_status;

    return;
}

void send_debbug_message (TE_VALUES *value, TE_DEBUG_MESSAGE *debug_message)
{
    debug_message->APPS_0 = value->avg_APPS_0;
    debug_message->APPS_1 = value->avg_APPS_1;
    debug_message->BPS_pressure_0 = value->avg_BPS_pressure_0;
    debug_message->BPS_pressure_1 = value->avg_BPS_pressure_1;

    return;
}

void send2CAN (TE_MESSAGE *main_message, TE_DEBUG_MESSAGE *debug_message)
{
    CANdata main_msg;
    CANdata debug_msg;

    main_msg.dev_id = DEVICE_ID_TE;
    main_msg.msg_id = MSG_ID_TE_MAIN;
    main_msg.dlc = 8;
    main_msg.data[0] = main_message->status.TE_status;
    main_msg.data[1] = main_message->APPS;
    main_msg.data[2] = main_message->BPS_pressure;
    main_msg.data[3] = main_message->BPS_electric;

    if(main_message->status.Debug_Mode)
    {
        debug_msg.dev_id = DEVICE_ID_TE;
        debug_msg.msg_id = MSG_ID_TE_DEBUG;
        debug_msg.dlc = 8;
        debug_msg.data[0] = debug_message->APPS_0;
        debug_msg.data[1] = debug_message->APPS_1;
        debug_msg.data[2] = debug_message->BPS_pressure_0;
        debug_msg.data[3] = debug_message->BPS_pressure_1;
    }

    //CANout_place (&main_msg);
    write_to_can2_buffer (main_msg, 1);
    if(main_message->status.Debug_Mode)
    {
        //CANout_place (&debug_msg);
        write_to_can2_buffer (debug_msg, 0);
    }
    return;
}

void send_raw_values(TE_VALUES *values){

    CANdata raw_message_1;
    CANdata raw_message_2;

    raw_message_1.dev_id = DEVICE_ID_TE;
    raw_message_1.msg_id = MSG_ID_TE_RAW_1;
    raw_message_1.dlc = 8;

    raw_message_1.data[0] = values->raw_avg_APPS_0;
    raw_message_1.data[1] = values->raw_avg_APPS_1;
    raw_message_1.data[2] = values->raw_avg_BPS_pressure_0;
    raw_message_1.data[3] = values->raw_avg_BPS_pressure_1;
    
    raw_message_2.dev_id = DEVICE_ID_TE;
    raw_message_2.msg_id = MSG_ID_TE_RAW_2;
    raw_message_2.dlc = 2;

    raw_message_2.data[0] = values->raw_avg_BPS_electric;

    write_to_can2_buffer (raw_message_1, 0);
    write_to_can2_buffer (raw_message_2, 0);

    return;
}

void send_messages (TE_VALUES *value, TE_CORE *status)
{
    TE_MESSAGE main_message;
    TE_DEBUG_MESSAGE debug_message;
    
    //Prepares the main CAN message to be sent
    send_status         (status, &main_message);
    send_APPS           (value, status, &main_message);
    send_BPS_pressure   (value, status, &main_message);
    send_BPS_eletric    (value, status, &main_message);

    if(status->Debug_Mode)
        send_debbug_message (value, &debug_message);

    if(status->Raw_Mode)
        send_raw_values(value);
       
    send2CAN (&main_message, &debug_message);


    return;
}

void send_BPS_pressure_0_analog (TE_CORE *status) {

    if (status->CC_BPS_pressure_0)
        LATDbits.LATD2 = 0;
    else 
        LATDbits.LATD2 = 1;

    return;
}

void send_init_msg (void)
{
    CANdata init_msg;

    init_msg = make_reset_msg(DEVICE_ID_TE, RCON);
    
    write_to_can2_buffer (init_msg, 1);

    return;
}


bool filter_can2(uint16_t sid)
{
    switch(CAN_GET_DEV_ID(sid)){
        case DEVICE_ID_INTERFACE:
            switch(CAN_GET_MSG_ID(sid)){
                case CMD_ID_INTERFACE_DEBUG_TOGGLE:
                    return true;
                    break;
                case CMD_ID_INTERFACE_SET_PEDAL_THRESHOLD:
                    return true;
                    break;
                case CMD_ID_INTERFACE_TE_TOGGLE_RAW_MODE:
                    return true;
                    break;
                default:
                    return false;
                    break;
            }
        break;

        case DEVICE_ID_DASH:
           
            if(CAN_GET_MSG_ID(sid) == CMD_ID_COMMON_RTD_ON){
                return true;
            }
            else 
                return false;
        break;

        default:
            return false;
    }
}

void process_can_message (CANdata *r_message, 
                          TE_CORE *status, 
                          TE_VALUES *values, 
                          _prog_addressT EE_address,
                          uint16_t EE_thresholds[16],
                          TE_THRESHOLDS *thresholds){

    INTERFACE_CAN_Data interface_data;

    switch (r_message->dev_id){

        case DEVICE_ID_INTERFACE:

            parse_can_interface(*r_message, &interface_data);

            switch (r_message->msg_id){

                case CMD_ID_INTERFACE_DEBUG_TOGGLE:
              
                    if(r_message->data[0] == DEBUG_TOGGLE_TORQUE_ENCODER)
                        status->Debug_Mode ^= 1;                     
                    
                    break;
                
                case CMD_ID_INTERFACE_SET_PEDAL_THRESHOLD:

                        update_selected_mechanical_thresholds (interface_data.pedal_thresholds, 
                                                               values, 
                                                               EE_address, 
                                                               EE_thresholds, 
                                                               thresholds); 
                        //LATBbits.LATB12 = 0;
                    break;

                case CMD_ID_INTERFACE_TE_TOGGLE_RAW_MODE:

                        status->Raw_Mode ^= 1;

                    break;

                default:
                    break;
            }
        
        break;
  
        case DEVICE_ID_DASH:
            LATBbits.LATB12 = 0;
            if(r_message->msg_id == CMD_ID_COMMON_RTD_ON){

                //LATBbits.LATB12 = 0;
                COMMON_MSG_RTD_ON RTD_sequence;
                parse_can_common_RTD(*r_message, &RTD_sequence);

                if(RTD_sequence.step == RTD_STEP_DASH_BUTTON){

                    CANdata RTD_TE_message;
                
                    RTD_TE_message.dev_id = DEVICE_ID_TE;
                    RTD_TE_message.msg_id = CMD_ID_COMMON_RTD_ON;
                    RTD_TE_message.dlc = 2;

                    if(check_TE_RTD_status(status, values))
                        RTD_TE_message.data[0] = RTD_STEP_TE_OK;
                    else
                        RTD_TE_message.data[0] = RTD_STEP_TE_NOK;

                    write_to_can2_buffer(RTD_TE_message, 1);
                } 

                

                  
            }
        break;  

        default:
            break;
    }
}